<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Conekta;

class WebController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
        {
            //guardas todo
            $nombre = $request->input('nombre');
            $email = $request->input('email');
            $tipo_pago = $request->input('tipo_pago');
            $token_id = $request->input('conektaTokenId');

            \Conekta\Conekta::setApiKey(env('CONEKTA_PRIVATE_KEY'));
            \Conekta\Conekta::setApiVersion("2.0.0");
            $line_items = array();

            //Simulamos los productos de la compra
            for($i = 0; $i<2; $i++)
            {
                $modelo = "Producto $i";
                $precio = 200;
                $cantidad = 1;

                $array_zapato = array(
                    "name" => $modelo,
                    "unit_price" => $precio * 100,
                    "quantity" => $cantidad
                );

                array_push($line_items, $array_zapato);
            }

            /*
            |
            | Formato calle
            |
            */
            $calle = 'Calle';
            $num_ext = '101-A';
            $num_int = null;
            $colonia = 'Colonia';

            $street = $calle.' '.$num_ext;
            if($num_int)
            {
                $street = $street . ' no. int. '.$num_int;
            }
            $street = $street . ' Col. '.$colonia;

            $estado = 'Guanajuato';
            $ciudad = 'León';
            $cp = '37000';

            /*
            |
            | Formato conekta
            |
            */

            /*Simulamos datos de la compra*/
            $folio = '0'.date('YmdHis'); //un folio o id interno de la compra
            $total = 500;
            $envio = 100;

            $order_array = array(
                "line_items" => $line_items,
                "shipping_lines" => array(
                    array(
                        "amount" => $envio * 100,
                        "carrier" => "UPS"
                    )
                ),
                "currency" => "MXN",
                "customer_info" => array(
                    "name" => $nombre,
                    "email" => $email,
                    "phone" => '4761133570' //Importante que venga en formato de 10 dígitos
                ),
                "shipping_contact" => array(
                    "phone" => '4761133570',
                    "receiver" => $nombre,
                    "email" => $email,
                    "address" => array(
                        'street1'=> $street,
                        'city'=> $ciudad,
                        'state'=> $estado,
                        'zip'=> $cp,
                        'country'=> 'México',
                        'postal_code' => $cp,
                        "residential" => true
                    )
                ),
                'metadata'    => array('reference' => $folio),
                "amount" => $total * 100,
                'reference'=> $folio,
            );

            if($tipo_pago == 'oxxo')
            {
                try {

                    $expires_at =  new \DateTime('now', new \DateTimeZone('America/Mexico_City'));
                    $expires_at->modify('+3 day');
                    $fechaLimitePago = $expires_at->format('Y-m-d');

                    $order_array['charges'] = array(
                        array(
                            "payment_method" => array(
                                "type" => "oxxo_cash",
                                'expires_at' => strtotime($fechaLimitePago)
                            )
                        )
                    );


                    $conekta_charge = \Conekta\Order::create($order_array);
                    $referencia_conekta = $conekta_charge->id;
                    $referencia_oxxo = $conekta_charge->charges[0]->payment_method->reference;

                    echo 'Fecha limite de pago:'. $fechaLimitePago.'<br>'; //para oxxo la fecha límite de pago
                    echo 'Referencia conekta:'. $referencia_conekta.'<br>'; //Guardarlo en la base de datos como referencia
                    echo 'Referencia de pago en oxxo:'. $referencia_oxxo.'<br>'; //La referencia con la que el cliente va a oxxo y paga

                    /*
                        Si el cliente paga en oxxo Conekta nos avisa por webhook, hasta aquí el ecommerce termina y le dice al cliente que gracias por su compra, ya si paga el resto se hace por webhook
                    */

                }catch (\Conekta\ParameterValidationError $error){
                    $mensaje = $error->getMessage();
                    echo "Error de parámetros: $mensaje";
                } catch (\Conekta\Handler $error){
                    $mensaje = $error->getMessage();
                    echo "Error: $mensaje";
                }

                return;
            }


            //Este ejemplo es sin 3DS
            if($tipo_pago == 'tarjeta')
            {
                try {
                    //code...
                    $order_array['charges'] = array(
                        array(
                            "payment_method" => array(
                                "type" => "card",
                                'token_id' => $token_id
                            )
                        )
                    );
                    $conekta_charge = \Conekta\Order::create($order_array);
                    $referencia_conekta = $conekta_charge->id;

                    echo 'Referencia conekta:'. $referencia_conekta.'<br>'; //Guardarlo en la base de datos como referencia
                    echo "Estatus del pago: ". $conekta_charge->payment_status.'<br>';
                    echo '<pre>';
                    print_r($conekta_charge); //Aquí pueden ver el objeto completo para ver que info necesitan guardar
                    echo '</pre>';

                }catch (\Conekta\ParameterValidationError $error){
                    $mensaje = $error->getMessage();
                    echo "Error de parámetros: $mensaje";
                } catch (\Conekta\Handler $error){
                    $mensaje = $error->getMessage();
                    echo "Error: $mensaje";
                }
                return;
            }
        }
        else
        {
            return view('index');
        }
    }
}
