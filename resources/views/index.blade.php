<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <form action="{{ route('index') }}" method="POST" id="card-form">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <div>
                <label>
                    <span>Nombre</span>
                    <input type="text" name="nombre">
                </label>
            </div>
            <div>
                <label>
                    <span>Correo</span>
                    <input type="text" name="email">
                </label>
            </div>
            <div>
                <label>
                    <span>Tipo de pago</span>
                    <select name="tipo_pago">
                        <option value="tarjeta">Tarjeta</option>
                        <option value="oxxo">OXXO</option>
                    </select>
                </label>
            </div>
            <span class="card-errors"></span>
            <div>
                <label>
                    <span>Nombre del tarjetahabiente</span>
                    <input type="text" size="20" data-conekta="card[name]">
                </label>
            </div>
            <div>
                <label>
                    <span>Número de tarjeta de crédito</span>
                    <input type="text" size="20" data-conekta="card[number]">
                </label>
            </div>
            <div>
                <label>
                    <span>CVC</span>
                    <input type="text" size="4" data-conekta="card[cvc]">
                </label>
            </div>
            <div>
                <label>
                    <span>Fecha de expiración (MM/AAAA)</span>
                    <input type="text" size="2" data-conekta="card[exp_month]">
                </label>
                <span>/</span>
                <input type="text" size="4" data-conekta="card[exp_year]">
            </div>
            <button>Pagar</button>
        </form>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>
        <script type="text/javascript" >
            Conekta.setPublicKey('{{ env("CONEKTA_PUBLIC_KEY") }}');


            var conektaSuccessResponseHandler = function(token) {
                var $form = $("#card-form");
                //Inserta el token_id en la forma para que se envíe al servidor
                $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
                $form.get(0).submit(); //Hace submit
            };
            var conektaErrorResponseHandler = function(response) {
                var $form = $("#card-form");
                $form.find(".card-errors").text(response.message_to_purchaser);
                $form.find("button").prop("disabled", false);
            };


            //jQuery para que genere el token después de dar click en submit
            $(function () {
                $("#card-form").submit(function(event) {
                var $form = $(this);
                // Previene hacer submit más de una vez
                $form.find("button").prop("disabled", true);
                Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
                return false;
                });
            });
            </script>
    </body>
</html>
